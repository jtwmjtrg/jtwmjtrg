#include "Manager.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	ChangeWindowMode(true); //ウィンドウモード

	// ＤＸライブラリ初期化処理
	if (DxLib_Init() == -1)
	{
		return -1;        // エラーが起きたら直ちに終了
	}

	// 描画先を裏画面にする
	SetDrawScreen(DX_SCREEN_BACK);

	XINPUT_STATE input; //ゲームパッド

	bool endflag = false; //終了処理のフラグ

	Manager* mgr = new Manager();

	while (ProcessMessage() != -1 && endflag == false) //メインループ
	{
		ClearDrawScreen(); //画面のクリアー

		// 入力状態を取得
		GetJoypadXInputState(DX_INPUT_PAD1, &input);

		mgr->gamemain(input); //実際に稼働する部分

		ScreenFlip(); //表へ描写

		if (CheckHitKey(KEY_INPUT_ESCAPE) == 1 || (input.Buttons[4] == 1 && input.Buttons[5] == 1))
		{
			endflag = true;
		}
	}

	delete mgr;

	// ＤＸライブラリ使用の終了処理
	DxLib_End();

	return 0;            // ソフトの終了
}