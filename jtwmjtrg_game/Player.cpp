#include "Player.h"

Player::Player(){
	playergraph = LoadGraph("img\\playergraph.png");
	playerx = 0;
	playery = 0;
	direction = 3;
	hp = 1;
	speed = 4; //移動スピード
	jumpflag = false; //ジャンプ中かどうかを示すフラグ
	max_jumppower = 40;
	decreace_jumppower = 3;
	jumppower = max_jumppower; //ジャンプでどれだけ高く上がるか
	gravity = 8;
}
Player::~Player()
{
	DeleteGraph(playergraph);
}

void Player::move(XINPUT_STATE input)
{	
	//移動
	if ((CheckHitKey(KEY_INPUT_LEFT) == 1 || input.Buttons[2] == 1)) //左へ
	{
		//左へ移動して向きを左に
		if (playerx - speed < 0)
		{

		}
		else
		{
			playerx -= speed;
		}

		direction = 2;
	}
	else if ((CheckHitKey(KEY_INPUT_RIGHT) == 1 || input.Buttons[3] == 1)) //右へ
	{
		//右に移動して向きを右に
		playerx += speed;
		direction = 3;
	}

	//ジャンプ
	if (CheckHitKey(KEY_INPUT_SPACE) == 1 || input.Buttons[12] == 1)
	{
		jumpflag = true;
	}
	if (jumpflag == true)
	{
		playery -= jumppower;
		jumppower -= 2;
	}
}

void Player::correction()
{
	switch (direction) //向きによる横の当たり判定
	{
	case 0:
		break;
	case 1:
		break;
	case 2:
		playerx += 64-playerx%64; //位置修正
		break;
	case 3:
		playerx -= playerx%64; //位置修正
		break;
	default:
		//マップ外だが処理上0と同じ扱い
		break;
	}
}

void Player::falling(int foot)
{
	switch (foot) //縦の当たり判定
	{
	case 0:
		playery += gravity; //自由落下
		break;
	case 1:
		playery -= (playery % 64);// ますぴったりの位置へ
		jumppower = max_jumppower; //ジャンプパワー初期化
		jumpflag = false; //ジャンプ処理終了
		break;
	case 2:
		playery -= (playery % 64);// ますぴったりの位置へ
		jumppower = max_jumppower; //ジャンプパワー初期化
		jumpflag = false; //ジャンプ処理終了
		break;
	default:
		//マップ外だが処理上0と同じ扱い
		playery += gravity; //自由落下
		break;
	}
}

void Player::draw()
{
	if (playerx < 320)
	{
		DrawGraph(playerx, playery-480, playergraph, true);
	}
	else
	{
		DrawGraph(320, playery-480, playergraph, true);
	}	
}
void Player::draw(int x, int y)
{
	
}

int Player::get_playerx()
{
	return playerx;
}
void Player::set_playerx(int playerx)
{
	this->playerx = playerx;
}
int Player::get_playery()
{
	return playery;
}
void Player::set_playery(int playery)
{
	this->playery = playery;
}