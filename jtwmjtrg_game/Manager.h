#pragma once
#include "Title.h"
#include "Stage.h"

class Manager
{
private:
	Title* title; //Title型オブジェクトtitle
	Stage* stage; //Stage型オブジェクトstage

	int gamescene; //ゲームの進行状態を数値で表す
	int pre_gamescene; //直前のゲームシーン
public:
	Manager();
	~Manager();

	void gamemain(XINPUT_STATE input); //事実上の稼働部分
};