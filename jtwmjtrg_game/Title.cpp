#include "Title.h"

Title::Title()
{
	background = LoadGraph("img\\title_background.png");
	bgm = LoadSoundMem("sound\\title_bgm.wav");
	selecticon = LoadGraph("img\\title_selecticon.png");
	
	keywait = re_keywait;

	selectx = 0;
	selecty = 0;
	nextmode = 0;
}
Title::~Title()
{
	DeleteGraph(background);
	DeleteSoundMem(bgm);
	DeleteGraph(selecticon);
}

void Title::modeselect(XINPUT_STATE input)
{
	if (keywait == 0) //�A�˖h�~
	{
		if ((CheckHitKey(KEY_INPUT_LEFT) == 1 || input.Buttons[2] == 1) && selectx > 0) //����
		{
			selectx--;
			keywait = re_keywait;
		}
		else if ((CheckHitKey(KEY_INPUT_RIGHT) == 1 || input.Buttons[3] == 1) && selectx < 1) //�E��
		{
			selectx++;
			keywait = re_keywait;
		}
	}
	else //�A�˖h�~
	{
		keywait--;
	}
}

void Title::draw()
{
	DrawGraph(0, 0, background, true);
	DrawGraph(selectx * 320, selecty, selecticon, true);
}
void Title::draw(int x, int y) //����Ȃ�
{
	DrawGraph(x, y, background, true);
}

int Title::get_nextmode()
{
	nextmode = selectx + 1;
	return nextmode;
}