#include "Map.h"

Map::Map()
{
	maphight = 16;
	mapwidth = 50; //マップのチップ数

	mapdata.resize(maphight); //マップのリサイズ
	for (int i = 0;i < (int)mapdata.size();i++)
	{
		mapdata[i].resize(mapwidth);
	}
	ifs.open("map\\mapdata.txt"); //オープン
	char c = 0; //一時保管
	for (int i = 0;i < maphight;i++)
	{
		for (int j = 0;j < mapwidth;j++)
		{
			ifs.get(c);
			mapdata[i][j] = ((int)c) - 48; //ASCII調整
		}
		ifs.get(c); //改行パス
	}

	flashgraph = 0;
	map_background = LoadGraph("img\\map_background.png");
	map_soil = LoadGraph("img\\map_soil.png");
	map_stone = LoadGraph("img\\map_stone.png");
	map_warp = LoadGraph("img\\map_warp.png");
}
Map::~Map()
{
	DeleteGraph(map_background);
	DeleteGraph(map_soil);
	DeleteGraph(map_stone);
}

void Map::draw()
{

}
void Map::draw(int x, int y)
{
	DrawGraph(0, 0 - y, map_background, true); //背景

	for (int i = 0;i < maphight;i++)
	{
		for (int j = 0;j < mapwidth;j++)
		{
			flashgraph = 0;

			switch (mapdata[i][j])
			{
			case 0: //何もなし
				break;
			case 1: //土
				flashgraph = map_soil;
				break;
			case 2: //石
				flashgraph = map_stone;
				break;
			case 3: //ワープ
				flashgraph = map_warp;
				break;
			default:
				break;
			}

			DrawGraph(j * 64 - x, i * 64 - y, flashgraph, true);
		}
	}
}

int Map::get_mapdata(int x, int y)
{
	return mapdata[x][y];
}