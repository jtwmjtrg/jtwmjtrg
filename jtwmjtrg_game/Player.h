#pragma once
#include "Drawwable.h"
#include "DxLib.h"

class Player : public Drawable
{
private:
	int playergraph; //画像
	int playerx, playery; //座標
	int direction; //向いてる方向当たり判定用
	int hp; //hp
	int speed; //移動スピード
	int max_jumppower; //リセット用
	int decreace_jumppower; //ジャンプの緩さ
	bool jumpflag; //ジャンプ中かどうかを示すフラグ
	int jumppower; //ジャンプでどれだけ高く上がるか
	int gravity; //重力
public:
	Player();
	~Player();

	void move(XINPUT_STATE input); //入力受付

	void draw();
	void draw(int x, int y); //継承部分の実装

	void correction(); //当たり判定
	void falling(int foot); //足元の当たり判定

	int get_playerx();
	void set_playerx(int playerx);
	int get_playery();
	void set_playery(int playery);
};