#include "Stage.h"

Stage::Stage()
{
	map = new Map();
	player = new Player();

	nextmode = 1;

	camerax = 0;
	cameray = 0;

	correctionloop = false;

	for (int i = 0;i < 2;i++)
	{
		playerx[i] = 0;
		playery[i] = 0;
		playerfoot[i] = 0;
	}
	
}
Stage::~Stage()
{

}

void Stage::move(XINPUT_STATE input)
{	
	//プレイヤーの処理
	player->move(input); //移動プロセス

	for (int i = 0;i < 2;i++) //移動プロセス後の位置取得
	{
		playery[i] = (int)((player->get_playery() + (i * 64)) / 64);
		playerfoot[i] = (int)((player->get_playerx() + 1 + (i * 62)) / 64);
	}

	//足元の当たり判定
	if (map->get_mapdata(playery[1], playerfoot[0]) == 0 && map->get_mapdata(playery[1], playerfoot[1]) == 0)
	{
		player->falling(0);
	}
	else if (map->get_mapdata(playery[1], playerfoot[0]) == 3 || map->get_mapdata(playery[1], playerfoot[1]) == 3)
	{
		nextmode = 0;
	}
	else
	{
		player->falling(1);
	}

	for (int i = 0;i < 2;i++) //移動プロセス後の位置取得
	{
		playerx[i] = (int)((player->get_playerx() + (i * 64)) / 64);
		playerside[i] = (int)((player->get_playery() + 32) / 64);
	}
	correctionloop = false;
	for (int i = 0;i < 2;i++)
	{
		for (int j = 0;j < 2;j++)
		{
			//横の当たり判定
			switch (map->get_mapdata(playerside[i], playerx[j]))
			{
			case 0: //何もなし
				break;
			case 1: //地面
				player->correction();
				correctionloop = true;
				break;
			case 2: //壁
				player->correction();
				correctionloop = true;
				break;
			case 3: //ワープ
				nextmode = 0;
				break;
			default:
				//エラー
				break;
			}
			if (correctionloop == true)
			{
				break;
			}
		}
		if (correctionloop == true)
		{
			break;
		}
	}
	//ここまでプレイヤーの処理

	camerax = player->get_playerx();
	cameray = player->get_playery(); //カメラ補正

}

void Stage::draw()
{
	if (camerax < 320)
	{
		map->draw(0, 480);
	}
	else
	{
		map->draw(camerax-320, 480);
	}
	player->draw();
}
void Stage::draw(int x, int y)
{

}

int Stage::get_nextmode()
{
	return nextmode;
}