#pragma once
#include "Drawwable.h"
#include "Map.h"
#include "Player.h"

class Stage : public Drawable
{
private:
	Map *map; //Map型オブジェクトmap
	Player* player; //Player型オブジェクトplayer

	int nextmode; //Managerに渡すゲーム進行を表す変数

	//カメラ
	int camerax, cameray; //補正の量

	//当たり判定のループ脱出フラグ
	bool correctionloop;
	
	//一時保管
	int playerx[2], playery[2], playerfoot[2], playerside[2];
public:
	Stage();
	~Stage();

	void move(XINPUT_STATE input);

	void draw();
	void draw(int x, int y); //継承部分の実装

	int get_nextmode();
};