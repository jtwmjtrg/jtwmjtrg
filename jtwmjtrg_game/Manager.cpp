#include "Manager.h"

Manager::Manager()
{
	title = new Title();

	gamescene = 0;
	pre_gamescene = 0;
}
Manager::~Manager() {}

void Manager::gamemain(XINPUT_STATE input)
{
	pre_gamescene = gamescene; //直前のシーンを保存

	switch (gamescene)
	{
	case 0:
		title->modeselect(input);
		title->draw();
		if (CheckHitKey(KEY_INPUT_RETURN) == 1 || input.Buttons[12] == 1)
		{
			gamescene = title->get_nextmode(); //Aボタンで次のシーンへ
		}
		break;
	case 1:
		stage->move(input);
		stage->draw();
		if (stage->get_nextmode() != 1)
		{
			gamescene = 0; //仮
		}
		break;
	default:
		//エラー
		break;
	}

	if (pre_gamescene != gamescene) //シーンが切り替わっていたらオブジェクトの削除生成
	{
		switch (pre_gamescene)
		{
		case 0:
			delete title;
			break;
		case 1:
			delete stage;
			break;
		default:
			break;
		}
		switch (gamescene)
		{
		case 0:
			title = new Title();
			break;
		case 1:
			stage = new Stage();
			break;
		default:
			break;
		}
	}
}