#pragma once
#include "Drawwable.h"
#include "DxLib.h"

class Title : public Drawable
{
private:
	int background; //背景
	int bgm; //BGM

	int keywait; //連射防止
	const int re_keywait = 16; //リセット
	int selectx, selecty,selecticon; //選択アイコンの座標、画像
	int nextmode; //Managerに渡すゲーム進行を表す変数
public:
	Title();
	~Title();

	void modeselect(XINPUT_STATE input); //入力受け付けてゲームを進行させる

	void draw();
	void draw(int x, int y);  //継承部分の実装

	int get_nextmode();
};