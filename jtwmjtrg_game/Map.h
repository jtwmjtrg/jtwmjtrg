#pragma once
#include "Drawwable.h"
#include "DxLib.h"
#include <iostream>
#include <vector>
#include <fstream>

using std::ifstream;
using std::vector;

class Map
{
private:
	ifstream ifs; //ファイル操作
	int maphight = 16;
	int mapwidth = 50; //マップのチップ数
	vector<vector<int> > mapdata; //マップデータ
	
	int flashgraph; //一時保管
	int map_background; //背景
	int map_soil; //マップチップ
	int map_stone; //マップチップ
	int map_warp; //マップチップ
public:
	Map();
	~Map();

	void draw();
	void draw(int x, int y); //継承部分の実装

	int get_mapdata(int i, int j);
};