#pragma once

class Drawable
{
public:
	virtual void draw() = 0; //引数なし
	virtual void draw(int x, int y) = 0; //＋座標
};